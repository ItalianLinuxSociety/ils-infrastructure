# Docker Guidelines for Italian Linux Society

## Dockerfile

The file `Dockerfile` - if any - should `EXPOSE` only the official well-known port (like, 8080 for Tomcat, or 8000 for Laravel, etc.)
and not any non-official port.

The file `Dockerfile` should use a non-root `USER`.

The file `Dockerfile` should not have an entrypoint that prints the internal port. This may be super-confusing for newcomers. If you cannot print also the right external port for the consumers, better to omit that specific information from your output (unless debugging mode, etc).

## compose.yaml

The file `compose.yaml` should also expose only the official well-known port, as the `Dockerfile`.

The file `compose.yaml` should work even if an `.env` file is not available.

The file `compose.yaml` should listen only on the loopback interface as default for security reasons.

The `compose.yaml` file should be under version control.

The `compose.yaml` should be flexible enough to cover most workflows, without local changes.

The `compose.yaml` should be customizable from the `.env` file. See the next section.

✅Correct `compose.yaml` for Laravel (so it also work without an `.env` file with the official Laravel port):

```
...
  ports:
    - "${SOMETHING_PORT:-127.0.0.1:8000}:8000"
...
```

❌Wrong `compose.yaml` (use the `.env` file to adopt non-official ports for Laravel):

```
...
  ports:
    - "${SOMETHING_PORT:-127.0.0.1:9999}:8000"
...
```

## Environment file

The file `.env` should not be under version control.

## Example environment file

The file `.env.example` should be available in each repository, under version control.

The file `.env.example` should have a plug-and-play configuration. So, if possible, avoiding port collisions among other well-known repositories (in the same organization at least).

The file `.env.example` should have "namespaced" ports to avoid collisions, respecting the following practical registry of well-known ports from the same organization:

| Project     | Namespaced port | Intended port | Purpose               |
|-------------|-----------------|-------------- |-----------------------|
| Linux.it    | ` 8000`         | `8000`        | Nikola serve          |
| ILS.org     | `18000`         | `8000`        | Nikola serve          |
| ILS Manager | `28000`         | `8000`        | Laravel Artisan serve |
