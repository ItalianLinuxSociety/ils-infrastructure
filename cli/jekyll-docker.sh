#!/bin/bash
################################################################
# Build a static HTML website with Jekyll with Docker.
################################################################
# DESCRIPTION
#
# This script MUST be run inside a valid Jekyll repository.
#
################################################################
# IMPORTANT
#
# You SHOULD export your important Jekyll environment variables
# before executing this script.
#
################################################################
# EXAMPLE USAGE
#
#     cd my-repo
#     export JEKYLL_ENV=production
#     /opt/ils-infrastructure/cli/build-jekyll.sh
#
################################################################
# LICENSE
#
# Copyright (C) 2022, 2023 Valerio Bozzolan, contributors
# MIT License
################################################################

# exit in case of any error
set -e

# the Jekyll primary command is ... all the arguments
JEKYLL_PRIMARY_COMMAND=$@

# Internal port on the Docker container
# (this MUST be hardcoded to 4000)
JEKYLL_INTERNAL_PORT=4000

function help() {
	echo "Usage: "
	echo "  $0 jekyll build"
	echo "Another usage:"
	echo "  $0 jekyll serve"
	echo
	echo "Available environment variables:"
	echo "  JEKYLL_VERSION (string) Version of the Docker container"
	echo "  JEKYLL_ENV     (string) Jekyll environment to generate     (example: 'production')"
	echo "  JEKYLL_PORT    (int)    Port on your host for Jekyll serve (default $JEKYLL_INTERNAL_PORT)"
	echo "  JEKYLL_UID     (int)    Unix user ID                       (default to your UID)"
	echo "  JEKYLL_GID     (int)    Unix user GID                      (default to your GID)"
}

function error_help() {
	help
	echo
	echo "ERROR:"
	echo "  $1"
	exit 1
}

# no command no party
if [ -z "$JEKYLL_PRIMARY_COMMAND" ]; then
	error_help "Missing primary command"
fi

#
# Arguments related to the Docker --publish option
#
# this is not present for builds, in order to allow
# parallel builds in the same environment
#
# This should be "" (empty) during normal builds
# This should be something like "--publish 4000:4000" for serve
DOCKER_PUBLISH_PARAM=

# Automatically detect whenever the user is running a "jekyll serve"
# This is important since we do NOT want to occupy all the time a port just to do a build.
# Having multiple build-jobs listening on a port causes parallel work to be interrupted.
# Essentially this is what a local developer expects on their computer.
if [[ "$JEKYLL_PRIMARY_COMMAND" =~ .+" serve".* ]]; then

	# Jekyll port eventually exposed on the host
	if [ -z "$JEKYLL_PORT" ]; then
		JEKYLL_PORT="$JEKYLL_INTERNAL_PORT"
	fi

	# whenever Docker should listen to a specific interface
	# (empty as default)
	if [ -n "$JEKYLL_IFACE" ]; then
		# if the input is "[::1]" it becomes "[::1]:", in order to end with ":"
		JEKYLL_IFACE="$JEKYLL_IFACE"":"
	fi

	# Argument for the Docker "--publish" parameter
	# normally this should something like:       "4000:4000"
	# or, with the interface, it becomes:  "[::1]:4000:4000"
	DOCKER_PUBLISH_ARG="$JEKYLL_IFACE""$JEKYLL_INTERNAL_PORT":"$JEKYLL_PORT"

	# Complete Docker parameter to publish an internal port to an external one
	DOCKER_PUBLISH_PARAM="--publish $DOCKER_PUBLISH_ARG"
fi

# no version no party
if [ -z "$JEKYLL_VERSION" ]; then
	error_help "Missing environment variable JEKYLL_VERSION"
fi

# no version no party
if [ -z "$JEKYLL_ENV" ]; then
	error_help "Missing environment variable JEKYLL_ENV"
fi

# as default pass the current User ID inside the Docker container
# to avoid permission issues on stuff mounted in the container
if [ -z "$JEKYLL_UID" ]; then
	# for example this is "1000" for the first non-root normal user
	JEKYLL_UID=$(id -u)
fi

# as default pass the current Group ID inside the Docker container
# to avoid permission issues on stuff mounted in the container
if [ -z "$JEKYLL_GID" ]; then
	# for example this is "1000" for the first non-root normal user
	JEKYLL_GID=$(id -g)
fi

# Credits for the Jekyll Docker image:
# https://github.com/envygeeks/jekyll-docker/blob/master/README.md
# https://hub.docker.com/r/jekyll/jekyll/
#
# Docker parameters:
#
#   run             Executes the command "jekyll build"
#                   using the Docker image "jekyll/builder:..."
#                   Note that the exact image name is influenced
#                   by the environment variable $JEKYLL_VERSION
#     --rm          Remove the container after execution
#     --tty=true    Allocates a pseudo-TTY
#     --interactive Keep STDIN open
#     --volume (1)  Mount the current directory ($PWD) inside the
#                   container on /srv/jekyll in order to build
#                   the static website.
#     --volume (2)  Mount part of the vendor directory (in $PWD)
#                   inside the container, to have a persistent
#                   cache for the bundle stuff.
#     :Z            The volume ':Z' option tells Docker to label
#                   content with a private unshared label.
#                   Private volumes can only be used by the
#                   current container.
#     --env=        Pass this environment variable inside the
#                   container. The variable JEKYLL_ENV is used
#                   to set the correct build profile.

# showing debug stuff
echo "command:        $JEKYLL_PRIMARY_COMMAND"
echo "JEKYLL_ENV:     $JEKYLL_ENV"
echo "JEKYLL_UID:     $JEKYLL_UID"
echo "JEKYLL_GID:     $JEKYLL_GID"
echo "JEKYLL_VERSION: $JEKYLL_VERSION"

if [ -z "$DOCKER_PUBLISH_PARAM" ]; then
	echo "JEKYLL_PORT:    (ignoring when not serving)"
else
	echo "JEKYLL_PORT:    $JEKYLL_PORT"
	echo "                $DOCKER_PUBLISH_ARG"
fi

# debug Bash commands
set -x

# build Jekyll from the current working directory
docker run                         \
	--rm                        \
	--tty=true                   \
	--interactive                 \
	--volume="$PWD:/srv/jekyll:Z"  \
        --volume="$PWD/vendor/bundle:/usr/local/bundle:Z" \
        $DOCKER_PUBLISH_PARAM            \
	--env JEKYLL_ENV="$JEKYLL_ENV"    \
	--env JEKYLL_UID="$JEKYLL_UID"     \
	--env JEKYLL_GID="$JEKYLL_GID"      \
	jekyll/builder:"$JEKYLL_VERSION"     \
	$JEKYLL_PRIMARY_COMMAND

# clear custom Bash flag
set +x
