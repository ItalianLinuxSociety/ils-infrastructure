#!/bin/sh


# preparation:
# apt update && apt install -y sassc
# ln -s /usr/bin/sassc /usr/bin/sass
# pip install virtualenv
# virtualenv venv
# source venv/bin/activate
# pip install Nikola[extras] pyaml

# TODO: eventually put in a Docker command
virtualenv venv
. venv/bin/activate
pip install Nikola[extras] pyaml

nikola plugin -i sass

nikola "$@"
