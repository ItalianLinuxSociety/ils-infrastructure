# Hardware and Software Inventory - Italian Linux Society

This is a keep-it-simple-and-stupid public inventory, shared with everybody, with the following goals:

- to have a glimpse of what servers and services are available to Italian Linux Society
- simplify auditing and identify bus factors
- make GDPR happier
- to quickly ping specific humans in case of disasters
- just have fun documenting things in a stupid way
- yet another great use of a TXT file without bells and whistles

Thanks for any contribution to this document, hoping to be useful to you! ❤️

## Websites Inventory

Nonsense order - feel free to sort:

| URL                      | Software       | Server               | Main Contacts       | Super Admins         | Auto Backup   |
|--------------------------|----------------|----------------------|---------------------|----------------------|---------------|
| https://www.linux.it/    | static         | `itgate-scotty-worf` | gnu, madbob, mte90  | (server admins)      | ✅ not needed |
| https://www.linux.it/stickers/| static    | `itgate-scotty-worf` | ferdi2005           | (server admins)      | see ILS Manager |
| https://www.linuxday.it/ | static         | `itgate-scotty-worf` | gnu, madbob, mte90  | (server admins)      | ✅ not needed |
| https://www.ils.org/     | static         | `itgate-scotty-worf` | gnu, madbob, mte90  | (server admins)      | ✅ not needed |
| https://*.ils.org/       |WordPress multi | `itgate-scotty-worf` | madbob              | (server admins)      | ❌ ? | 
| https://ilsmanager.linux.it/| Laravel, PHP| `itgate-scotty-ilsmanager`| madbob, gnu    |                      | ❌ ? |
| https://lists.linux.it/  | Mailman        | `itgate-scotty-picard`| md                 | madbob               | ❌ ? |
| https://forum.linux.it   |Discourse, Python| `garr-discourse`  | mte90     | oirasor, mte90       | ❌ just on-site? |
| https://video.linux.it/  |PeerTube|`garr-peertube` + `scaleway`| oirasor        | gnu, mte90, madbob   | ❌ ? |
| https://lugmap.linux.it/ | PHP            | `itgate-scotty-kirk` | gelma, madbob       |                      | ✅ not needed |
| https://crm.linux.it/    | Mautic         | `garr-ils-docker-1`| oirasor        | madbob, mte90, gnu   | ❌ ? |
| https://merge-it.net/    | PHP            | `hetz-servizi`     | madbob, mte90, gnu  | (server admins)      | ✅ not needed |
|https://online.merge-it.net/| static       | `hetz-servizi`     | mte90               | (server admins)      | ✅ not needed | 
| https://planet.linux.it/ | PHP            | `hetz-servizi`     | mte90, madbob       | (server admins)      | ✅ not needed |
| https://kirk.linux.it/roundcube/|Roundcube| `itgate-scotty-kirk`      | md                  |                      | ✅ not needed |
| https://stats.linux.it/  | Matomo, PHP    | `garr-ils-docker-1`| gnu, mte90, oirasor, loviuz | madbob        | ❌ ? |
| https://servizi.linux.it/ | Sandstorm     | `hetz-servizi`     | madbob, gnu, oirasor| mte90                | ❌ ? |
| https://login.servizi.linux.it/|SimpleSamlPhp| `hetz-servizi`  | mte90               | mte90                | ❌ ? |
| https://sistemainoperativo.it | static    | `hetz-servizi`      | madbob, gnu         | (server admins)      | ✅ not needed |
| https://linuxsi.com/     | PHP            | `hetz-servizi`      | madbob              | (server admins)      | ✅ not needed |
| https://scuola.linux.it/ | static         | `itgate-scotty-worf`  | madbob              | (server admins)      | ✅ not needed |
| LOCAL PROJECTS *.ils.org |Worpress multisite|`itgate-scotty-worf`| mte90, gnu          |                      |      |
| - https://alessandria.ils.org/|WordPress multisite|`itgate-scotty-worf`| angelo.pistamiglio|                  | ❌ ? | 
| - https://bari.ils.org/  |WordPress multisite|`itgate-scotty-worf`| ferdi2005          |                      | ❌ ? | 
| - https://este.ils.org/  | static         |`itgate-scotty-worf`  | loviuz              |                      | ✅ not needed |
| - https://perugia.ils.org/|WordPress multisite|`itgate-scotty-worf`|francesco.marinucci|                      | ❌ ? | 
| - https://rieti.ils.org/  |WordPress multisite|`itgate-scotty-worf`|mte90              |                      | ❌ ? | 
| - https://torino.ils.org/ |WordPress multisite|`itgate-scotty-worf`|madbob, gnu        | oirasor              | ❌ ? | 
| LOCAL PROJECTS *.linux.it|                |               |                     |                      |      |
| - http://appunti.linux.it/|               | 85.94.199.210 |                     |                      | ❌ ? |
| - https://belluno.linux.it/|static        | `itgate-scotty-kirk` |                     |                      | ❌ ? |
| - https://catania.linux.it| static        | (external)    |                     |                      |      |
| https://debian.linux.it/ | static         | `itgate-scotty-kirk` | md                  |                      | ✅ not needed |
| - https://enna.linux.it |                 | (external)    | nocs                |                      |      |
| - http://erlug.linux.it |                 | (external)    |                     |                      |      |
| - https://este.linux.it/| static          | `itgate-scotty-kirk` | loviuz              |                      | ❌ ? |
| - https://farezero.linux.it/| WordPress   | `itgate-scotty-kirk` |                     |                      | ❌ ? |
| - https://ferrara.linux.it/ | Joomla!     | (external)    |                     |                      |      |
| - https://firenze.linux.it/|WordPress     | `itgate-scotty-kirk` | matteobin           |                      | ❌ ? |
| - https://ftp.linux.it/  |                | `itgate-scotty-kirk` | md, gnu             | (server admins)      | ✅ not needed |
| - https://gimp.linux.it/ | static         | `itgate-scotty-kirk` |                     |                      | ❌ ? |
| - https://golem.linux.it/| -              | (external)    | giomba              |                      |      |
| - https://gulp.linux.it/ |WordPress       | (external)    |                     |                      |      |
| -- http://linuxday2008.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2009.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2010.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2011.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2012.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2013.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2014.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2015.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2016.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2017.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2018.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2019.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2020.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2022.gulp.linux.it/|    | (external)    |                     |                      |      |
| -- http://linuxday2023.gulp.linux.it/|    | (external)    |                     |                      |      |
| - http://lugbs.linux.it/ | static         | (external)    |                     |                      |      |
| -- http://risotto.lugbs.linux.it/ |WordPress|(external)   |                     |                      |      |
| - https://pavia.linux.it/| WordPress      | `itgate-scotty-kirk` |              |                      |      |
| - https://pdp.linux.it/  | WordPress      | (external)    |                     |                      |      |
| - https://relug.linux.it | MediaWiki      | `itgate-scotty-kirk` |              |                      | ❌ ? |
| - http://siena.linux.it/ | PHP            | (external)    |                     |                      |      |
| - https://siracusa.linux.it/|static       |`itgate-scotty-kirk`  |              |                      | ❌ ? | 
| - https://trieste.linux.it/| WordPress    | (external)    |                     |                      |      |
| - https://tp.linux.it/   | static         | `itgate-scotty-kirk` | `md`, `gnu`  | (server admins)      | ✅ not needed |
| - https://wlog.linux.it/ |                | `itgate-scotty-kirk` |              |                      | ❌ ? |
| https://gimpitalia.it/   | PHP            |`garr-gimp`    |                     | `gnu`, `oirasor`     | ❌ ? |
| xmpp.linux.it            |                |`garr-firenze-poc`| `matteobin`      | `oirasor`            | ❌ ? |
| ..                       | ..             | ..               | ..               | ..                   | ..   |
| http://lilliput.linux.it/| mix            | (external)       | `gaio`           |                      | ✅ not needed |
|https://nextcloud.linux.it| Nextcloud      | `hetzner`     | `mte90`             | `gnu`                | ✅  |

**Source code**: the source code and the documentation of each service should be self-evident from the website itself. If not, edit the website itself. If the website is offline, visit Internet Archive :D Most repositories are here: https://gitlab.com/ItalianLinuxSociety/ Thanks to any bug report.

**Backup**: This column is just here just to stress. Should be green only if "not needed" (that means, the project has no personal data to be restored from a backup). The column should be SCARYING AND RED IN ANY OTHER CASE since backups are never enough.

**Super Admins**: additional people with internal knowledge AND full access to that application. Not general sysadmins already listed in the related Server. The list is intended as contact fallback, but also for GDPR purposes.

IMPORTANT: Where you see **external** the service is entirely maintained by an entity outside Italian Linux Society. Contact that external entity for information about data protection.

## Servers Inventory

| Server        | VPS                      | WAN IP           | LAN IP         |OS               |Maintainer  |Additional Root Sysadmins| Provider | Auto Backup / Snapshots |
|---------------|--------------------------|------------------|----------------|-----------------|------------|---------------|----|--|
|               |`garr-k8s-1`              | //               |`192.168.4.121` |✅CentOS Stream 9|kowalski7cc|gnu        | `garr` | no |
|               |`garr-k8s-2`              | //               |`192.168.4.???` |✅CentOS Stream 9|kowalski7cc|gnu        | `garr` | no |
|               |`garr-ils-docker-1`       | `90.147.188.235` |                |⚠️ CentOS 8       |oirasor   | mte90, gnu | `garr` | ? |
|               |`garr-gimp`               | `90.147.189.21`  |                |⚠️ Debian bullseye| oirasor  | gnu        | `garr` | ? |
|               |`garr-discourse`          | `90.147.189.104` |                |⚠️ CentOS 7       | oirasor  | mte90, gnu | `garr` | ? |
|               |`garr-peertube`           | `90.147.188.223` |                |⚠️ CentOS 8       | oirasor  | gnu, mte90 | `garr` | ? |
|               |`garr-firenze-poc`        | `90.147.189.115` |                |✅Debian bookworm|matteobin |oirasor, gnu| `garr` | ? |
|               |`hetz-servizi`            | `195.201.99.72`  |                |⚠️ Debian stretch | madbob   | gnu, mte90, oirasor | `hetzner` | ? |
|`itgate-scotty`|`itgate-scotty-ilsmanager`| `213.254.12.147` |                |⚠️ Debian buster  | gnu      | madbob       | `itgate` | ? |
|`itgate-scotty`|`itgate-scotty-picard`    | `213.254.12.146` |                |✅Debian bookworm| md       | madbob, fabulus, gnu (non-root) | `itgate` | ? |
|`itgate-scotty`|`itgate-scotty-worf`      | `213.254.12.150` |                |⚠️ Debian buster  | md       | madbob, gnu | `itgate` | ? |
|`itgate-scotty`|`itgate-scotty-kirk`      | `213.254.12.151` |                |⚠️ Debian buster  | md       | madbob, fabulus, gnu | `itgate` | ? |

Notes:

- the server `itgate-scotty` is a bare metal server `HP DL360 G6` that should be decommissioned and replaced (https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/issues/15)

The operating system for new servers SHOULD be:

- Debian GNU/Linux stable

The operating system for new servers MAY be:

- Rocky Linux stable

Ideally every server should be easily restorable from a snapshot, and, we should have off-site backups.

OS: Operating Systems. Outdated operating systems are flagged. Outdated operating systems are frequent since we do not have infinite time. Updating the OS is a priority, but often consider that the application maybe already up to date, so an outdated OS may be a minor problem, in the absence of known security issues about in-use OS components.

## Social Networks / External Websites

| URL                                         | Main Contact | Additional Admins |
|---------------------------------------------|--------------|-------------------|
| https://facebook.com/italinuxsociety        | gnu          | mte90 |
| https://facebook.com/LinuxDayItalia         | madbob       | |
| https://gitlab.com/ItalianLinuxSociety/     | gnu, madbob  | see [ILS GitLab members](https://gitlab.com/groups/ItalianLinuxSociety/-/group_members) |
| https://gitlab.com/merge-it                 | madbob, mte90| see [MERGE-it GitLab members](https://gitlab.com/groups/merge-it/-/group_members) |
| https://mastodon.uno/@ItaLinuxSociety       | segreteria   | gnu, mte90 |
| https://mobilizon.it/@italian_linux_society | https://t.me/olazza | https://t.me/lost, gnu |
| https://mobilizon.it/ `@ils`                | gnu          | direttore |
| https://mobilizon.it/ `@mergeit`            | gnu          | direttore |
| https://t.me/ItalianLinuxSociety            | gnu, mte90   | see yourself from Telegram |
| https://twitter.com/ItaLinuxSociety         | mte90, gnu   | |
| https://twitter.com/lugmap                  | madbob, gelma| rubi, md, fabulus |
| https://twitter.com/linuxdayitalia          | madbob       | |
| https://twitter.com/merge_it                | madbob       | |
| https://hub.docker.com/u/italianlinuxsociety| gnu          | oirasor |

Please propose stub posts for our social networks! Anyone can propose posts, for example here:

https://t.me/ItalianLinuxSociety/444

## Mailboxes @linux.it @ils.org

All mailboxes and aliases are maintained by the glorious `md`.

All active members have their own address. This section only documents special aliases and special mailboxes. Aliases have no password.

| Email alias   | Destination | Purpose                                                |
|---------------|-------------|--------------------------------------------------------|
| `assistenza@` | `forum+assistenza@` | Land in https://forum.linux.it/c/assistenza/12 |
| `direttore@`  | →           | see https://www.ils.org/info/#consiglio-direttivo      |
| `ils-cd@`     | →           | see https://www.ils.org/info/#consiglio-direttivo)     |
| `presidente@` | →           | see https://www.ils.org/info/#consiglio-direttivo)     |
| `segreteria@` | `presidente` + `direttore` | receive invoices                        |
| `webmaster@`  | `direttore` + `gnu` + `mte90` | quick tech contact                   |

Special mailboxes:

- `nonrispondere`
    - in use by https://ilsmanager.linux.it/
    - in use by https://servizi.linux.it/admin/email
    - in use by https://crm.linux.it/s/config/edit
    - in use by ... (probably more things)

## Domains

| Domain               | Provider  |
|----------------------|-----------|
| gimpitalia.it        | `netsons` |
| ils.org              | `netsons` |
| linux.it             | `netsons` |
| linuxday.it          | `netsons` |
| linuxday.it          | `netsons` |
| linuxsi.com          | `netsons` |
| lugmap.it            | `netsons` |
| merge-it.net         | `netsons` |
| opendidattica.it     | `netsons` |
| opendidattica.org    | `netsons` |
| sistemainoperativo.it| `netsons` |

## DNS

List of people that can change DNS records on domains.

| Domain           | Provider | Admins      |
|------------------|----------|-------------|
| linux.it         | `itgate` | `md`        |
| ils.org          | `itgate` | `md`        |
| firenze.linux.it |`garr` (`firenze`)|`matteobin`|

## Service Providers

Various service providers and suppliers.

| Provider  | Provides                | Main Contacts   | Core Notifications | Payment       | Legal Notes       |
|-----------|-------------------------|-----------------|--------------------|---------------|-------------------|
| `garr`    | complimentary OpenStack | madbob, oirasor, gnu |               |*complimentary*| Partnership ends in 2026 (!) |
| `hetzner` | VPS                     | `gnu`           | `segreteria`       | PayPal        | ESTEROMETRO (Germany) |
| `itgate`  | bare metal              | md              |                    |*complimentary*| Partnership? |
| `netsons` | domains                 | madbob          |                    | ?             | Pescara. |
| `scaleway`|object storage (PeerTube)| gnu, oirasor    | `segreteria`       | `gnu` (!)     | ESTEROMETRO (France). No personal data. Only public videos. |
| `flug`    | for Firenze user group  | matteobin       |                    |*complimentary*| Firenze. Self-hosting their stuff :3 |
|`flyeralarm` | prints, stickers      | gnu             | `segreteria`       | ?             | Foreign supplier? → "esterometro"? No personal data. Only public shipping addresses. |
|`spedirepro`|package shipping LinuxDay|gnu             | `webmaster`        | PayPal        | |
|`stickermule`| stickers, gadgets     | gnu             | `segreteria`       | PayPal        | Bologna. No personal data. Only public shipping addresses. |

(!) issues:

- https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/21 - garr
- https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/20 - scaleway

## Banks

For obvious reasons, the president (`gnu`) and director (`loviuz`) must have access to bank accounts.

In 2021 we converted all accounts to "business" type, therefore, there are no shared secrets. We have only personal accounts.

Bank accounts:

* Unicredit (Business)
* PayPal Business

## Broken Stuff (to be Killed)

Probably nobody does not care about these. Probably to be killed (or restored? nah, let's kill. Or restore? Who knows).

1. https://fosdem.linux.it/ (502 Bad Gateway) - linked from https://login.servizi.linux.it/
2. https://donazioni.linux.it/
3. https://sicurezza.linux.it/
4. http://venezia.linux.it/ (domain parked?)

## History

We have no idea how we got to this point.

## How to Read this Document

Maintainers are listed in "please contact me" order. Put yourself first if you want to be contacted.

Services are listed in a "order by server, importance, alphabetical" order. I mean, random. Feel free to re-sort.

## Bugs / Questions / Contact

Every person can be contacted via email using `NICKNAME@linux.it` if needed.

## Domande

Eh? Contattaci :)

https://www.ils.org/contatti/
